<?php
include ('cek.php');
error_reporting(0);
session_start();
?>

<?php
include('cek_level.php');
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Peminjaman</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">
	
	<link href="../css/bootstrap.min.css" rel="stylesheet">
	<link href="../css/bootstrap-responsive.min.css" rel="stylesheet">
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
	<link href="../css/font-awesome.css" rel="stylesheet">
	<link href="../css/style.css" rel="stylesheet">
	<link href="../css/pages/dashboard.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="assets/css/jquery.dataTables.css">
</head>
<body>
<div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container"> <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span> </a><a class="brand" href="#">INVENTARIS SEKOLAH</a>
      <div class="nav-collapse">
        <ul class="nav pull-right">
          <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-user"></i> <?php echo $_SESSION['nama_petugas'];?> <b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="#logout" role="button" data-toggle="modal">Keluar</a></li>
            </ul>
          </li>
        </ul>
      </div>
      <!--/.nav-collapse --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /navbar-inner --> 
</div>
<!-- /navbar -->

<!-- Modal Logout -->
<div class="modal small fade" id="logout" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h3 id="myModalLabel">Konfirmasi Logout</h3>
			</div>
			<div class="modal-body">
				<p class="error-text"><i class="fa fa-warning modal-icon"></i>Anda yakin ingin keluar dari website ini?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Kembali</button>
				<a class="btn btn-warning" href="logout.php">Keluar</a>
			</div>
		</div>
	</div>
</div>
<!-- /logout -->

<!-- nav -->
<div class="sidebar-nav">
	<?php       
		if ($_SESSION['id_level']==1){
			echo '
			<div class="subnavbar">
			  <div class="subnavbar-inner">
				<div class="container">
				  <ul class="mainnav">
					<li><a href="index.php"><i class="icon-dashboard"></i><span>Dashboard</span> </a> </li>
					<li><a href="inventaris.php"><i class="icon-list-alt"></i><span>Inventaris</span> </a> </li>
					<li class="active"><a href="peminjaman.php"><i class="icon-upload-alt"></i><span>Peminjaman</span> </a></li>
					<li><a href="pengembalian.php"><i class="icon-download-alt"></i><span>Pengembalian</span> </a> </li>
					<li><a href="laporan.php"><i class="icon-download-alt"></i><span>Laporan</span> </a> </li>
					<li class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-long-arrow-down"></i><span>Lainnya</span> <b class="caret"></b></a>
					  <ul class="dropdown-menu">
						<li><a href="jenis.php">Jenis</a></li>
						<li><a href="ruang.php">Ruang</a></li>
						<li><a href="petugas.php">Petugas</a></li>
						<li><a href="pegawai.php">Pegawai</a></li>
					  </ul>
					</li>
				  </ul>
				</div>
				<!-- /container --> 
			  </div>
			  <!-- /subnavbar-inner --> 
			</div>
			<!-- /subnavbar -->
			';
		}elseif ($_SESSION['id_level']==2){
			echo'
		    <div class="subnavbar">
			  <div class="subnavbar-inner">
				<div class="container">
				  <ul class="mainnav">
					<li><a href="index.php"><i class="icon-dashboard"></i><span>Dashboard</span> </a> </li>
					<li class="active"><a href="peminjaman.php"><i class="icon-upload-alt"></i><span>Peminjaman</span> </a></li>
					<li><a href="pengembalian.php"><i class="icon-download-alt"></i><span>Pengembalian</span> </a> </li>
				  </ul>
				</div>
				<!-- /container --> 
			  </div>
			  <!-- /subnavbar-inner --> 
			</div>
			<!-- /subnavbar -->
			';
	   }elseif ($_SESSION['id_level']==3){
		   echo'
		   <div class="subnavbar">
			  <div class="subnavbar-inner">
				<div class="container">
				  <ul class="mainnav">
					<li><a href="index.php"><i class="icon-dashboard"></i><span>Dashboard</span> </a> </li>
					<li class="active"><a href="peminjaman.php"><i class="icon-upload-alt"></i><span>Peminjaman</span> </a></li>
				  </ul>
				</div>
				<!-- /container --> 
			  </div>
			  <!-- /subnavbar-inner --> 
			</div>
			<!-- /subnavbar -->
			';
	   }
	?>   
</div>
<!-- /nav -->

<?php
	if($_SESSION['id_level'] == 1): ?>
	<?php echo "
	<div class='control-group'>
		<div class='alert alert-success'>
			<button type='button' class='close' data-dismiss='alert'>&times;</button>
			Selamat Datang Admin<br>Di Website Inventaris Sarana dan Prasarana SMK ini. Disini tersedia menu Peminjaman dan Pengembalian untuk anda.
		</div>
	</div>
	";
	?>
<?php endif; ?>
<?php
	if($_SESSION['id_level'] == 2): ?>
	<?php echo "
	<div class='control-group'>
		<div class='alert alert-success'>
			<button type='button' class='close' data-dismiss='alert'>&times;</button>
			Selamat Datang Operator<br>Di Website Inventaris Sarana dan Prasarana SMK ini. Disini tersedia menu Peminjaman dan Pengembalian untuk anda.
		</div>
	</div>
	";
	?>
<?php endif; ?>
<?php
	if($_SESSION['id_level'] == 3): ?>
    <?php echo "
	<div class='control-group'>
		<div class='alert alert-success'>
			<button type='button' class='close' data-dismiss='alert'>&times;</button>
			Selamat Datang Peminjaman<br>Di Website Inventaris Sarana dan Prasarana SMK ini. Disini tersedia menu Peminjaman dan Pengembalian untuk anda.
		</div>
	</div>
	";
	?>
<?php endif; ?>

<div class="main">
  <div class="main-inner">
    <div class="container">
	  <div class="row"> 	
		<div class="span12">
		  <div class="widget">
		    <div class="widget-header">
				<i class="icon-th-large"></i>
				<h3>LAPORAN</h3>
			</div> <!-- /widget-header -->
			<div class="widget-content">
			<div class="panel-body">
				<div class="table-responsive">
				<table id="example" class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>ID Peminjaman</th>
											<th>Tanggal Pinjam</th>
									<th>Tanggal Kembali</th>
                                            <th>Status Peminjaman</th>
                                            <th>ID Pegawai</th>
											<th>Option</th>
                                        </tr> 
                                    </thead>
									<tbody>
									<?php
                                    include "koneksi.php";
                                   $select=mysql_query("select * from peminjaman");
                                    while($data=mysql_fetch_array($select))
                                    {
                                    ?>
                                        <tr>
											<td><?php echo $data['id_peminjaman']; ?></td>
	                                        <td><?php echo $data['tanggal_pinjam']; ?></td>
											<td><?php echo $data['tanggal_kembali']; ?></td>
											<td><?php echo $data['status_peminjaman']; ?></td>
                                            <td><?php echo $data['id_pegawai']; ?></td>
											<td><a class="btn outline btn-success icon-edit" href="editinfo.php?id=<?php echo $data['id']; ?>"></a>
											<a class="btn btn outline btn-danger icon-trash" href="infohapus.php?id=<?php echo $data['id']; ?>"></a></td>
											
                                        </tr>
										<?php
									}
									?>
                                     
                                            
											
                                    </tbody>
                                </table>
</div>
				<!-- /table-responsive -->
			</div>
			<!-- /panel-body -->
			</div>
			<!-- /widget-content -->
		  </div>
		  <!-- /widget -->
		</div>
		<!-- /span12 -->
      </div>
	  <!-- /row -->
	</div>
	<!-- /container -->
  </div>
  <!-- /main-inner -->
</div>
<!-- /main -->

<div class="footer">
  <div class="footer-inner">
    <div class="container">
      <div class="row">
        <div class="span12"> &copy; 2019 Copyright <a href="#">Dwi Herliansyah</a>. </div>
        <!-- /span12 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /footer-inner --> 
</div>
<!-- /footer --> 
				  
<script type="text/javascript" src="assets/js/jquery.js"></script>
<script type="text/javascript" src="assets/js/jquery.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="assets/js/jquery.dataTables.min.js"></script>							
<script>
    $(document).ready(function() {
        $('#example').DataTable();
    });
</script>
<script src="lib/bootstrap/js/bootstrap.js"></script>
<script type="text/javascript">
        $("[rel=tooltip]").tooltip();
        $(function() {
            $('.demo-cancel-click').click(function(){return false;});
        });
</script>	
</body>
</html>