<!-- koneksi
 =======================-->
<?php
  include "koneksi.php";
  session_start();
  $nama = $_SESSION['nama'];
  $level = $_SESSION['level'];
  if(!isset($_SESSION['username'])){
	  header("location:login.php");
  }
  ?>
<!--tutup koneksi
 =======================-->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Ruang - Inventaris SMKN 1 Ciomas</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/pages/faq.css" rel="stylesheet"> 
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<body>
<div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container"> <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><span
      class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span> </a><a class="brand" href="index.html">INVENTARIS SMKN 1 CIOMAS</a>
      <div class="nav-collapse">
        <ul class="nav pull-right">
          <li class="dropdown">						
			<a href="#" class="dropdown-toggle" data-toggle="dropdown">
				<i class="icon-user"></i> 
				<?php echo $nama ?>
				<b class="caret"></b>
			</a>			
			<ul class="dropdown-menu">
				<li><a href="logout.php">Logout</a></li>
			</ul>						
		  </li>
        </ul>
      </div>
      <!--/.nav-collapse -->	  
    </div>
    <!-- /container --> 
  </div>
  <!-- /navbar-inner --> 
</div>
<!-- /navbar -->   
<div class="subnavbar">
	<div class="subnavbar-inner">
		<div class="container">
			<ul class="mainnav">
				<li><a href="index.php"><i class="icon-dashboard"></i><span>Dashboard</span> </a> </li>
				<li><a href="inventaris.php"><i class="icon-list-alt"></i><span>Inventaris</span> </a> </li>
				<li><a href="peminjaman.php"><i class="icon-bar-chart"></i><span>Peminjaman</span> </a> </li>
				<li class="active"><a href="pengembalian.php"><i class="icon-code"></i><span>Pengembalian</span> </a> </li>
				<li class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-long-arrow-down"></i><span>Lainnya</span> <b class="caret"></b></a>
				  <ul class="dropdown-menu">
					<li><a href="ruang.php">Ruang</a></li>
					<li><a href="jenis.php">Jenis</a></li>
					<li><a href="pricing.html">Pegawai</a></li>
					<li><a href="petugas.php">Petugas</a></li>
				  </ul>
				</li>
			</ul>
		</div> <!-- /container -->
	</div> <!-- /subnavbar-inner -->
</div> <!-- /subnavbar -->

<div class="row">	      	
	<div class="span12">     		
		<div class="widget ">
			<div class="widget-header">
	      		<i class="icon-user"></i>
	      		<h3>Peminjaman</h3>
	  		</div> <!-- /widget-header -->		
				<div class="widget-content">		
					<div class="tabbable">						
						<br>	
							<div class="tab-pane" id="formcontrols">
								<form method="post" action="simpanruang.php"><br>
									<table class="table table-striped table-bordered" width="300" border="0" align="center" cellpadding="4" cellspacing="1">
										<tr><td>Nama Ruang</td><td><input name="nama_ruang" type="text" id="nama_ruang" size="20"></td></tr>
										<tr><td>Kode Ruang</td><td><input name="kode_ruang" type="text" id="kode_ruang" size="20"></td></tr>
										<tr><td>Keterangan</td><td><input name="keterangan" type="text" id="keterangan" size="20"></td></tr>
										<tr><td></td><td></td></tr>
										<tr>
											<td></td><td><input name="simpan" type="submit" id="simpan" value="Simpan"></td>
										</tr>
									</table>
								</form>
		</div></div></div></div></div></div> <!-- /row -->
		
<div class="row">	      	
	<div class="span12">      		      		
		<div class="widget ">  			
	      	<div class="widget-header">
	   			<i class="icon-user"></i>
   				<h3>Peminjaman</h3>
	  		</div> <!-- /widget-header -->		
				<div class="widget-content">		
					<div class="tabbable">						
						<br>	
						<div class="tab-pane" id="formcontrols">
							<table class="table table-striped table-bordered" width="800" height="50" align="center" cellpadding="0" cellspacing="0">
								<tr>
									<th>Nama Ruang</th>
									<th>Kode Ruang</th>
									<th>Keterangan</th>	
								</tr>
							<?php
							 include "koneksi.php";
							 $sql=("select * from ruang");
							 $select=mysqli_query($conn,$sql);
							 while ($data=mysqli_fetch_array($select))
							{
							?>
								<tr>
									<td align="center"><?php echo $data['nama_ruang'] ?></td>
									<td align="center"><?php echo $data['kode_ruang'] ?></td>
									<td align="center"><?php echo $data['keterangan'] ?></td>
								</tr>
							<?php } ?>
							</table><br>
</div></div></div></div></div></div><!-- /row -->
        
<div class="footer">
	<div class="footer-inner">
		<div class="container">
			<div class="row">
    			<div class="span12">
    				&copy; 2013 <a href="http://www.egrappler.com/">Bootstrap Responsive Admin Template</a>.
    			</div> <!-- /span12 -->
    		</div> <!-- /row -->
		</div> <!-- /container -->
	</div> <!-- /footer-inner -->
</div> <!-- /footer -->
    
<!-- Le javascript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="js/jquery-1.7.2.min.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script src="js/faq.js"></script>
<script>
$(function () {
	$('.faq-list').goFaq ();
});
</script>
  </body>
</html>