<?php 
include ('cek.php');
error_reporting(0);
session_start();
include ('cek_level');
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Peminjaman</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">
	
	<link href="../css/bootstrap.min.css" rel="stylesheet">
	<link href="../css/bootstrap-responsive.min.css" rel="stylesheet">
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
	<link href="../css/font-awesome.css" rel="stylesheet">
	<link href="../css/style.css" rel="stylesheet">
	<link href="../css/pages/dashboard.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="assets/css/jquery.dataTables.css">
</head>
<body>
<div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container"> <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span> </a><a class="brand" href="#">INVENTARIS SEKOLAH</a>
      <div class="nav-collapse">
        <ul class="nav pull-right">
          <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-user"></i> <?php echo $_SESSION['nama_petugas'];?> <b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="#logout" role="button" data-toggle="modal">Keluar</a></li>
            </ul>
          </li>
        </ul>
      </div>
      <!--/.nav-collapse --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /navbar-inner --> 
</div>
<!-- /navbar -->

<!-- Modal Logout -->
<div class="modal small fade" id="logout" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h3 id="myModalLabel">Konfirmasi Logout</h3>
			</div>
			<div class="modal-body">
				<p class="error-text"><i class="fa fa-warning modal-icon"></i>Anda yakin ingin keluar dari website ini?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Kembali</button>
				<a class="btn btn-warning" href="logout.php">Keluar</a>
			</div>
		</div>
	</div>
</div>
<!-- /logout -->

<!-- nav -->
<div class="sidebar-nav">
	<?php       
		if ($_SESSION['id_level']==1){
			echo '
			<div class="subnavbar">
			  <div class="subnavbar-inner">
				<div class="container">
				  <ul class="mainnav">
					<li><a href="index.php"><i class="icon-dashboard"></i><span>Dashboard</span> </a> </li>
					<li><a href="inventaris.php"><i class="icon-list-alt"></i><span>Inventaris</span> </a> </li>
					<li class="active"><a href="peminjaman.php"><i class="icon-upload-alt"></i><span>Peminjaman</span> </a></li>
					<li><a href="pengembalian.php"><i class="icon-download-alt"></i><span>Pengembalian</span> </a> </li>
					<li><a href="laporan.php"><i class="icon-download-alt"></i><span>Laporan</span> </a> </li>
					<li class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-long-arrow-down"></i><span>Lainnya</span> <b class="caret"></b></a>
					  <ul class="dropdown-menu">
						<li><a href="jenis.php">Jenis</a></li>
						<li><a href="ruang.php">Ruang</a></li>
						<li><a href="petugas.php">Petugas</a></li>
						<li><a href="pegawai.php">Pegawai</a></li>
					  </ul>
					</li>
				  </ul>
				</div>
				<!-- /container --> 
			  </div>
			  <!-- /subnavbar-inner --> 
			</div>
			<!-- /subnavbar -->
			';
		}elseif ($_SESSION['id_level']==2){
			echo'
		    <div class="subnavbar">
			  <div class="subnavbar-inner">
				<div class="container">
				  <ul class="mainnav">
					<li><a href="index.php"><i class="icon-dashboard"></i><span>Dashboard</span> </a> </li>
					<li class="active"><a href="peminjaman.php"><i class="icon-upload-alt"></i><span>Peminjaman</span> </a></li>
					<li><a href="pengembalian.php"><i class="icon-download-alt"></i><span>Pengembalian</span> </a> </li>
				  </ul>
				</div>
				<!-- /container --> 
			  </div>
			  <!-- /subnavbar-inner --> 
			</div>
			<!-- /subnavbar -->
			';
	   }elseif ($_SESSION['id_level']==3){
		   echo'
		   <div class="subnavbar">
			  <div class="subnavbar-inner">
				<div class="container">
				  <ul class="mainnav">
					<li><a href="index.php"><i class="icon-dashboard"></i><span>Dashboard</span> </a> </li>
					<li class="active"><a href="peminjaman.php"><i class="icon-upload-alt"></i><span>Peminjaman</span> </a></li>
				  </ul>
				</div>
				<!-- /container --> 
			  </div>
			  <!-- /subnavbar-inner --> 
			</div>
			<!-- /subnavbar -->
			';
	   }
	?>   
</div>
<!-- /nav -->

<?php
	if($_SESSION['id_level'] == 1): ?>
	<?php echo "
	<div class='control-group'>
		<div class='alert alert-success'>
			<button type='button' class='close' data-dismiss='alert'>&times;</button>
			Selamat Datang Admin<br>Di Website Inventaris Sarana dan Prasarana SMK ini. Disini tersedia menu Peminjaman dan Pengembalian untuk anda.
		</div>
	</div>
	";
	?>
<?php endif; ?>
<?php
	if($_SESSION['id_level'] == 2): ?>
	<?php echo "
	<div class='control-group'>
		<div class='alert alert-success'>
			<button type='button' class='close' data-dismiss='alert'>&times;</button>
			Selamat Datang Operator<br>Di Website Inventaris Sarana dan Prasarana SMK ini. Disini tersedia menu Peminjaman dan Pengembalian untuk anda.
		</div>
	</div>
	";
	?>
<?php endif; ?>
<?php
	if($_SESSION['id_level'] == 3): ?>
    <?php echo "
	<div class='control-group'>
		<div class='alert alert-success'>
			<button type='button' class='close' data-dismiss='alert'>&times;</button>
			Selamat Datang Peminjaman<br>Di Website Inventaris Sarana dan Prasarana SMK ini. Disini tersedia menu Peminjaman dan Pengembalian untuk anda.
		</div>
	</div>
	";
	?>
<?php endif; ?>

<div class="main">
  <div class="main-inner">
    <div class="container">
	  <div class="row"> 	
		<div class="span12">
		  <div class="widget">
		    <div class="widget-header">
				<i class="icon-th-large"></i>
				<h3>PEMINJAMAN</h3>
			</div> <!-- /widget-header -->
			<div class="widget-content">
			<div class="panel-body">
				<div class="table-responsive">
					<center>
					<label>Pilih nama barang</label>
					<form method="POST">
						<div class="form-control">
							<select name="id_inventaris">
                                    <?php
                                    include "koneksi.php";
                                    $result = mysql_query("SELECT id_inventaris,nama from inventaris where jumlah > 0");
                                    while($row = mysql_fetch_assoc($result))
                                    {
                                        echo "<option value='$row[id_inventaris]'>$row[nama]</option>";
                                    } 
                                    ?>
							</select>
						</div>
                        <button type="submit" name="pilih" class="btn btn-info">Pilih</button>
                    </form>
					</center>
                    <?php
                    if(isset($_POST['pilih'])){?>
                      <form action="simpan_smtr_detail.php" method="post" enctype="multipart/form-data">
                       <?php
                       include "koneksi.php";
                       $id_inventaris=$_POST['id_inventaris'];
                       $select=mysql_query("select * from inventaris where id_inventaris='$id_inventaris'");
                       while($data=mysql_fetch_array($select)){
                        ?>
						<center>
                        <div class="form-control">
							<label>ID Inventaris</label>
							<input name="id_inventaris" type="text" placeholder="Masukan ID Inventaris" value="<?php echo $data['id_inventaris'];?>" autocomplete="off" maxlength="11" required="" readonly="readonly">
							<input name="id_detail_pinjam" type="hidden" placeholder="Masukan ID Inventaris" value="<?php echo $data['id_inventaris'];?>" autocomplete="off" maxlength="11" required="" readonly="readonly">
						</div>
						<div class="form-control">
							<label>Nama</label>
							<input name="" type="text" placeholder="Masukan ID Inventaris" value="<?php echo $data['nama'];?>" autocomplete="off" maxlength="11" required="" readonly="readonly">
						</div>      
						<div class="form-control">
							<label>Jumlah Barang</label>
							<input name="jumlah" type="text" placeholder="Masukan ID Inventaris" value="<?php echo $data['jumlah'];?>" autocomplete="off" maxlength="11" required="" readonly="readonly">
						</div>
						<div class="form-group">
                                            <label>Nama Pegawai</label>
                                          <select name="id_jenis" class="form-control m-bot15">
                            <option>---Pilih---</option>
                            <?php
                            include "koneksi.php";
                                //display values in combobox/dropdown
                            $result = mysql_query("SELECT id_pegawai,nama_pegawai from pegawai ");
                            while($row = mysql_fetch_assoc($result))
                            {
                                echo "<option>$row[id_pegawai].$row[nama_pegawai]</option>";
                            } 
                            ?>

                        </select>
                                        </div>
                                          
						<div class="form-control">
							<label>Jumlah Pinjam</label>
							<input name="jumlah_pinjam" required="" type="number" placeholder="Jumlah" autocomplete="off" maxlength="11">
						</div>
						<button type="submit" class="btn">Pinjam</button>
						</center><br>
                          <?php  } ?>
                      </form>
                  <?php } ?>
				  
              <form action="simpan_pinjam.php" method="post" role="form">
                <?php 
                $status_peminjaman="Pinjam";
                include "koneksi.php";
                $query = "SELECT max(kode_pinjam) as maxKode FROM peminjaman";
                $hasil = mysql_query($query);
                $data = mysql_fetch_array($hasil);
                $kode_barang = $data['maxKode'];
                $noUrut = (int) substr($kode_barang, 4, 4);
                $noUrut++;
                $char = "PNJ";
                $kode_barang = $char . sprintf("%04s", $noUrut);
                ?>
                <br><br>
                          <?php $tanggal_pinjam=gmdate("Y-m-d H:i:s",time()+60*60*7); 
                          $kode_pinjam=$_POST['kode_pinjam'];
                          $status_peminjaman='Pinjam';
                          $auto=mysql_query("select * from peminjaman order by id_peminjaman desc limit 1");
                          $no=mysql_fetch_array($auto);
                          $angka=$no['id_peminjaman']+1;  ?>
                          <div class="form-control">
							<label>Tanggal Pinjam</label>
							<input name="tanggal_pinjam" readonly type="text" placeholder="Masukan ID Inventaris" value="<?php echo $tanggal_pinjam;?>" autocomplete="off" maxlength="11">
                          </div>
                          <div class="form-control">
							<label>Kode Pinjam</label>
							<input name="kode_pinjam" readonly type="text" placeholder="Masukan ID Inventaris" value="<?php echo $kode_barang;?>" autocomplete="off" maxlength="11">
                          </div>
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>ID Detail Pinjam</th>
                                        <th>Nama Barang</th>
                                        <th>Jumlah</th>
                                        <th>Jumlah Pinjam</th>
                                        <th>Nama Pegawai</th>
                                        <th>Option</th>

                                    </tr> 
                                </thead>
                                <tbody>
                                    <?php
                                    include "koneksi.php";
                                    $select=mysql_query("select * from smtr_detail left join  
                                        inventaris on smtr_detail.id_inventaris=inventaris.id_inventaris");
                                    while($data=mysql_fetch_array($select))
                                    {
                                        ?>
                                        <tr>
                                           <td><input name="id_detail_pinjam[]" type="text" class="form-control" placeholder="Masukan ID Inventaris" value="<?php echo $angka;?>" autocomplete="off" maxlength="11" required="" readonly="readonly">
                                               <input name="tanggal_kembali" type="hidden" class="form-control" placeholder="Masukan ID Inventaris" autocomplete="off" maxlength="11" required="" readonly="readonly">
                                               <input name="id_peminjaman" type="hidden" class="form-control" placeholder="Masukan ID Inventaris" value="<?php echo $angka;?>" autocomplete="off" maxlength="11" required="" readonly="readonly">
                                               <input name="status_peminjaman" type="hidden" class="form-control" placeholder="Masukan ID Inventaris" value="<?php echo $status_peminjaman;?>" autocomplete="off" maxlength="11" required="" readonly="readonly">
                                               <input name="id_inventaris[]" id="id_inventaris" type="hidden" class="form-control" placeholder="Masukan ID Inventaris" value="<?php echo $data['id_inventaris'];?>" autocomplete="off" maxlength="11" required="" readonly="readonly">
                                           <td><input name=""  type="text" class="form-control" placeholder="Masukan ID Inventaris" value="<?php echo $data['nama'];?>" autocomplete="off" maxlength="11" required="" readonly="readonly">
                                           <td><input name="jumlah[]" type="text" class="form-control" placeholder="Masukan ID Inventaris" value="<?php echo $data['jumlah'];?>" autocomplete="off" maxlength="11" readonly="readonly">
                                           <td><input name="jumlah_pinjam[]" type="text" class="form-control" placeholder="Masukan ID Inventaris" value="<?php echo $data['jumlah_pinjam'];?>" autocomplete="off" maxlength="11" readonly="readonly"></td>
                                           <td><input  name="id_pegawai" autocomplete="off" class="form-control" value="<?php echo $_SESSION['id_pegawai'];?>.<?php echo $_SESSION['nama_pegawai'];?>" rows="3" placeholder="Pilih ID Pegawai" readonly="readonly">
                                               </input>
                                                    <td>
                                                       <a class="btn btn outline btn-info  glyphicon glyphicon-remove" href="hapus_smtr_detail.php?id_detail_pinjam=<?php echo $data['id_detail_pinjam']; ?>"></a></td>
                                                       <?php } ?>
                                               </tr>
                                           </tbody>
                                       </table>
                                       <br>
                                       <button type="submit" class="btn btn-warning">&nbsp;Pinjam</button>
                                       <hr><br>
                                       <br>
                                   </form>
                                   
                                            <table class="table">
                                              <thead>
                                                <tr>
                                                  <th>No</th>
                                                  <th>Kode Pinjam</th>
                                                  <th>Tanggal Pinjam</th>
                                                  <th>Nama Pegawai</th>
                                                  <th>Status Peminjaman</th>
                                                  <th>Option</th>
                                                  <th style="width: 3.5em;"></th>
                                              </tr> 
                                          </thead>
                                          <tbody>
                                           <?php
                                           include "koneksi.php";
                                           $select=mysql_query("select * from peminjaman s left join pegawai p on p.id_pegawai=s.id_pegawai
                                             where status_peminjaman='Pinjam'");
                                           $no=1;
                                           while($data=mysql_fetch_array($select))
                                           {
                                            ?>
                                            <tr>
                                             <td><?php echo $no++; ?></td>
                                             <td><?php echo $data['kode_pinjam'];?></td>
                                             <td><?php echo $data['tanggal_pinjam']; ?></td>
                                             <td><?php echo $data['nama_pegawai']; ?></td>
                                             <td><span class="label label-success"><?php echo $data['status_peminjaman']; ?></td>
                                                 <td><a class="btn outline btn-primary  icon-eye-open " href="detail_pinjam.php?id_peminjaman=<?php echo $data['id_peminjaman']; ?>"></a></td>

                                             </tr>
                                             <?php
                                         }
                                         ?>
                                     </tbody>
                                 </table>
				</div>
				<!-- /table-responsive -->
			</div>
			<!-- /panel-body -->
			</div>
			<!-- /widget-content -->
		  </div>
		  <!-- /widget -->
		</div>
		<!-- /span12 -->
      </div>
	  <!-- /row -->
	</div>
	<!-- /container -->
  </div>
  <!-- /main-inner -->
</div>
<!-- /main -->

<div class="footer">
  <div class="footer-inner">
    <div class="container">
      <div class="row">
        <div class="span12"> &copy; 2019 Copyright <a href="#">Dwi Herliansyah</a>. </div>
        <!-- /span12 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /footer-inner --> 
</div>
<!-- /footer --> 
				  
<script type="text/javascript" src="assets/js/jquery.js"></script>
<script type="text/javascript" src="assets/js/jquery.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="assets/js/jquery.dataTables.min.js"></script>							
<script>
    $(document).ready(function() {
        $('#example').DataTable();
    });
</script>
<script src="lib/bootstrap/js/bootstrap.js"></script>
<script type="text/javascript">
        $("[rel=tooltip]").tooltip();
        $(function() {
            $('.demo-cancel-click').click(function(){return false;});
        });
</script>	
</body>
</html>