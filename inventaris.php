 <!--koneksi database
  ======================-->
  <?php
  include "koneksi.php";
  session_start();
  $nama = $_SESSION['nama'];
  $level = $_SESSION['level'];
  if(!isset($_SESSION['username'])){
	  header("location:login.php");
  }
  ?>
 <!--tutup koneksi
  =======================-->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Inventaris - Inventaris SMKN 1 Ciomas</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/pages/plans.css" rel="stylesheet"> 
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
  </head>
<body>
<div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container"> <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><span
      class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span> </a><a class="brand" href="index.html">INVENTARIS SMKN 1 CIOMAS</a>
      <div class="nav-collapse">
        <ul class="nav pull-right">
          <li class="dropdown">						
			<a href="#" class="dropdown-toggle" data-toggle="dropdown">
				<i class="icon-user"></i> 
				<?php echo $nama ?>
				<b class="caret"></b>
			</a>			
			<ul class="dropdown-menu">
				<li><a href="logout.php">Logout</a></li>
			</ul>						
		  </li>
        </ul>
      </div>
      <!--/.nav-collapse -->	  
    </div>
    <!-- /container --> 
  </div>
  <!-- /navbar-inner --> 
</div>
<!-- /navbar -->   
<div class="subnavbar">
	<div class="subnavbar-inner">
		<div class="container">
			<ul class="mainnav">
				<li><a href="index.php"><i class="icon-dashboard"></i><span>Dashboard</span> </a> </li>
				<li class="active"><a href="inventaris.php"><i class="icon-list-alt"></i><span>Inventaris</span> </a> </li>
				<li><a href="peminjaman.php"><i class="icon-bar-chart"></i><span>Peminjaman</span> </a> </li>
				<li><a href="pengembalian.php"><i class="icon-code"></i><span>Pengembalian</span> </a> </li>
				<li class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-long-arrow-down"></i><span>Lainnya</span> <b class="caret"></b></a>
				  <ul class="dropdown-menu">
					<li><a href="ruang.php">Ruang</a></li>
					<li><a href="jenis.php">Jenis</a></li>
					<li><a href="pricing.html">Pegawai</a></li>
					<li><a href="petugas.php">Petugas</a></li>
				  </ul>
				</li>
			</ul>
		</div> <!-- /container -->
	</div> <!-- /subnavbar-inner -->
</div> <!-- /subnavbar -->
    
<div class="main">
	<div class="main-inner">
		<div class="container">
			<div class="row">      	
				<div class="span12">      		
					<div class="widget ">
						<div class="widget-header">
							<i class="icon-user"></i>
							<h3>Inventaris</h3>
						</div> <!-- /widget-header -->					                          
							<div class="widget-content">
							<!-- Button to trigger modal -->
								<a href="#myModal" role="button" class="btn" data-toggle="modal">Tambah</a>
								<div class="tabbable">						
								<br>
									<div class="tab-pane" id="formcontrols">
										<table class="table table-striped table-bordered" width="800" height="50" align="center" cellpadding="0" cellspacing="0">
											<tr>
												<th><center>No</th>
												<th><center>Nama</th>
												<th><center>Kondisi</th>
												<th><center>Keterangan</th>
												<th><center>Jumlah</th>
												<th><center>jenis</th>
												<th><center>Tanggal Register</th>
												<th><center>Ruang</th>
												<th><center>Kode Inventaris</th>
												<th><center>Petugas</th>
												<th><center>Aksi</th>
											</tr>
										<?php
										include "koneksi.php";
										 $no=1;
										 $sql=("select * FROM inventaris left join jenis on inventaris.id_jenis = jenis.id_jenis
																		 left join ruang on inventaris.id_ruang = ruang.id_ruang
																		 left join petugas on inventaris.id_petugas = petugas.id_petugas");
										 $select=mysqli_query($conn,$sql);
										 while ($data=mysqli_fetch_array($select))
										{
										?>
											<tr>
												<td align="center"><?php echo $no++ ?></td>
												<td align="center"><?php echo $data['nama'] ?></td>
												<td align="center"><?php echo $data['kondisi'] ?></td>
												<td align="center"><?php echo $data['keterangan'] ?></td>
												<td align="center"><?php echo $data['jumlah'] ?></td>
												<td align="center"><?php echo $data['nama_jenis'] ?></td>
												<td align="center"><?php echo $data['tanggal_register'] ?></td>
												<td align="center"><?php echo $data['nama_ruang'] ?></td>
												<td align="center"><?php echo $data['kode_inventaris'] ?></td>
												<td align="center"><?php echo $data['nama_petugas'] ?></td>
												<td align="center"><a class="btn outline btn-success fa fa-edit" href="edit_inventaris.php?id_inventaris=<?php echo $data['id_inventaris'] ?>">Edit</a>
																   <a class="btn btn outline btn-danger fa fa-trash-o" href="hapus_inventaris.php?id_inventaris=<?php echo $data['id_inventaris'] ?>">Hapus</a>
												</td>
											</tr>
										<?php } ?>
										</table>
		<!-- Modal -->
          <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
              <h3 id="myModalLabel">Tambah Data</h3>
            </div>
            <div class="modal-body">
				<form method="post" action="simpaninventaris.php">
					<table width="700" border="0" align="center" cellpadding="4" cellspacing="1">
						<tr><td>Nama</td><td><input name="nama" type="text" size="35"></td></tr>
						<tr><td>Kondisi</td><td><input name="kondisi" type="text" size="25"></td></tr>
						<tr><td>Keterangan</td><td><input name="keterangan" type="text" size="30"></td></tr>
						<tr><td>Jumlah</td><td><input name="jumlah" type="text" size="20"></td></tr>
						<tr><td>Jenis</td>
							<td>
								<select name="id_jenis">
									<option></option>
									<?php $konek = mysqli_connect('localhost','root','','ujikom'); $query = mysqli_query($konek,"select * from jenis"); ?>
									<?php foreach($query as $data) { ?>
									<option value="<?php echo $data['id_jenis'] ?>"><?php echo $data['nama_jenis'] ?></option>
									<?php } ?>
								</select>
							</td>
						</tr>
						<tr><td>Tanggal Registrasi</td><td><input name="tanggal_registrasi" type="date" id="tanggal_registrasi" size="20"></td></tr>
						<tr><td>Ruang</td>
							<td>
								<select name="id_ruang">
									<option></option>
									<?php $konek = mysqli_connect('localhost','root','','ujikom'); $query = mysqli_query($konek,"select * from ruang"); ?>
									<?php foreach($query as $data) { ?>
									<option value="<?php echo $data['id_ruang'] ?>"><?php echo $data['nama_ruang'] ?></option>
									<?php } ?>
								</select>
							</td>
						</tr>
						<tr><td>Kode Inventaris</td><td><input name="kode_inventaris" type="number" size="11"></td></tr>
						<tr><td>Petugas</td>
							<td>
								<select name="id_petugas" id="petugas">
									<option></option>
									<?php $konek = mysqli_connect('localhost','root','','ujikom'); $query = mysqli_query($konek,"select * from petugas"); ?>
									<?php foreach($query as $data) { ?>
									<option value="<?php echo $data['id_petugas'] ?>"><?php echo $data['nama_petugas'] ?></option>
									<?php } ?>
								</select>
							</td>
						</tr>
					</table>
				</table>
            </div>
            <div class="modal-footer">
              <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
              <button name="simpan" type="submit" class="btn btn-primary">Save changes</button>
            </div>
		   </form>
          </div>
		  <!-- /modal -->
		
		</div></div></div></div></div></div>
		</div></div></div>
		
<div class="footer">
	<div class="footer-inner">
		<div class="container">
			<div class="row">
    			<div class="span12">
    				&copy; 2013 <a href="http://www.egrappler.com/">Bootstrap Responsive Admin Template</a>.
    			</div> <!-- /span12 -->
    		</div> <!-- /row -->
		</div> <!-- /container -->
	</div> <!-- /footer-inner -->
</div> <!-- /footer -->
    

<!-- Le javascript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="js/jquery-1.7.2.min.js"></script>

<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>

  </body>

</html>
