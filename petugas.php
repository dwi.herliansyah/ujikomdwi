<?php
  include "koneksi.php";
  session_start();
  $nama = $_SESSION['nama'];
  $level = $_SESSION['level'];
  if(!isset($_SESSION['username'])){
	  header("location:login.php");
  }
  ?>
  <!--tutup koneksi
  =======================-->
<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Petugas - Inventaris SMKN 1 CIOMAS</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   
    
    <link href="css/pages/faq.css" rel="stylesheet"> 

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
  </head>

<body>

<div class="navbar navbar-fixed-top">
	<div class="navbar-inner">
		<div class="container">
			
			<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</a>
			
			<a class="brand" href="index.php">
				INVENTARIS SMKN 1 CIOMAS				
			</a>		
			
			<div class="nav-collapse">
				<ul class="nav pull-right">
					<li class="dropdown">						
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<i class="icon-user"></i> 
							<?php echo $nama ?>
							<b class="caret"></b>
						</a>
						
						<ul class="dropdown-menu">
							<li><a href="logout.php">Logout</a></li>
						</ul>						
					</li>
				</ul>
			</div><!--/.nav-collapse -->	
	
		</div> <!-- /container -->
		
	</div> <!-- /navbar-inner -->
	
</div> <!-- /navbar -->
    
<div class="subnavbar">

	<div class="subnavbar-inner">
	
		<div class="container">

			<ul class="mainnav">
			
				<li>
					<a href="index.php">
						<i class="icon-dashboard"></i>
						<span>Dashboard</span>
					</a>	    				
				</li>
				
				
				
				<li>
					<a href="inventaris.php">
						<i class="icon-list-alt"></i>
						<span>Inventaris</span>
					</a>    				
				</li>
				
				<li>					
					<a href="guidely.html">
						<i class="icon-facetime-video"></i>
						<span>App Tour</span>
					</a>  									
				</li>
                
                
                <li>					
					<a href="peminjaman.php">
						<i class="icon-bar-chart"></i>
						<span>Peminjaman</span>
					</a>  									
				</li>
                
            
                <li>					
					<a href="pengembalian.php">
						<i class="icon-code"></i>
						<span>Pengembalian</span>
					</a>  									
				</li>
				
				<li class="active dropdown">					
					<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
						<i class="icon-long-arrow-down"></i>
						<span>Lainnya</span>
						<b class="caret"></b>
					</a>	
				
					<ul class="dropdown-menu">
                    	<li><a href="ruang.php">Ruang</a></li>
						<li><a href="jenis.php">Jenis</a></li>
                        <li><a href="pegawai.php">Pegawai</a></li>
                        <li><a href="petugas.php">Petugas</a></li>
					 </ul>    				
				</li>
			
			</ul>		</div> <!-- /container -->
	
	</div> <!-- /subnavbar-inner -->

</div> <!-- /subnavbar -->
	<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Petugas</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						<div class="tabbable">						
						
								<div class="tab-pane" id="formcontrols">
		<form method="post" action="simpanpetugas.php"><br>
		<table class="table table-striped table-bordered" width="600" align="center" cellpadding="0" cellspacing="0">
			<table width="300" border="0" align="center" cellpadding="4" cellspacing="1">
				<tr><td>Id Petugas</td><td><input name="id_petugas" type="text" id="id_petugas" size="20"></td></tr>
				<tr><td>Username</td><td><input name="username" type="text" id="username" size="20"></td></tr>
				<tr><td>Nama Petugas</td><td><input name="nama_petugas" type="text" id="nama_petugas" size="20"></td></tr>
				<tr><td>Password</td><td><input name="password" type="text" id="password" size="20"></td></tr>
				<tr><td>Level</td>
				<td>
					<select name="id_level">
					<option>pilih</option>
							<?php $konek = mysqli_connect('localhost','root','','ujikom'); $query = mysqli_query($konek,"select * from level"); ?>
							<?php foreach($query as $data) { ?>
					<option value="<?php echo $data['id_level'] ?>"><?php echo $data['nama_level'] ?></option>
							<?php } ?>
					</select>
				</td><tr><td></td><td></td></tr>
				<tr>
					<td></td><td><input name="simpan" type="submit" id="simpan" value="Simpan"></td>
				</tr>
			</table>
			</td></tr>
		</table>
		</form>
</div></div></div></div></div></div>
		
	     <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Petugas</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						<div class="tabbable">						
					
								<div class="tab-pane" id="formcontrols">		
		<table class="table table-striped table-bordered" width="600" height="100" align="center" cellpadding="0" cellspacing="0">
				<tr>
					<th>Id Pegawai</th>
					<th>Username</th>
					<th>Password</th>
					<th>Nama Petugas</th>
					<th>Id Level</th>
				</tr>
			<?php
		     include "koneksi.php";
			 $sql=("select * from petugas inner join level on Petugas.id_level = level.id_level");
			 $select=mysqli_query($conn,$sql);
			 while ($data=mysqli_fetch_array($select))
			{
			?>
				<tr>
					<td align="center"><?php echo $data['id_petugas'] ?></td>
					<td align="center"><?php echo $data['username'] ?></td>
					<td align="center"><?php echo $data['password'] ?></td>
					<td align="center"><?php echo $data['nama_petugas'] ?></td>
					<td align="center"><?php echo $data['id_level'] ?></td>
				</tr>
			<?php } ?>
		</table><br>
		
			</div></div></div></div></div></div> <!-- /row -->
	
	    </div> <!-- /container -->
    
	</div> <!-- /main-inner -->
	    
</div> <!-- /main -->
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2013 <a href="http://www.egrappler.com/">Bootstrap Responsive Admin Template</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    

<!-- Le javascript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="js/jquery-1.7.2.min.js"></script>

<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script src="js/faq.js"></script>

<script>

$(function () {
	
	$('.faq-list').goFaq ();

});

</script>
  </body>

</html>
